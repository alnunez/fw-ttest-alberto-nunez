# Power Measurements to Azure IoT via ESP-32 and FreeRTOS

This project gathers electrical power readings from 4 [SIEMENS PAC3200](/docs/resources/SIEMENS-PAC3200-EN.pdf) power meters and sends them to Azure cloud using an ESP-32 as an IoT bridge. The firmware was based on the [Azure IoT middleware for FreeRTOS repo](https://github.com/Azure/azure-iot-middleware-freertos).

![Diagram of the solution](/docs/resources/solution-diagram.png "Solution Diagram")

## Hardware solution description

The solution presented used an ESP-32 board connected via WiFi to an access point. There are also 4 other ESP-32 boards that simulate the behavior of the power meter, using a slightly modified version of the [ESP-IDF ModBus server example](https://github.com/espressif/esp-idf/tree/v4.3/examples/protocols/modbus/tcp/mb_tcp_slave).

![Hardware used](/docs/resources/hardware.jpg "Hardware used")

There are 10 variables of interest that must be polled from each device as follows:

Modbus registers map

| Register Name          | Address | Function     |
| ---------------------- | ------- | ------------ |         
| Voltage 1-N            |     1   | 0x03 or 0x04 |
| Voltage 2-N            |     3   | 0x03 or 0x04 |
| Voltage 3-N            |     5   | 0x03 or 0x04 |
| Current 1              |    13   | 0x03 or 0x04 |
| Current 2              |    15   | 0x03 or 0x04 |
| Current 3              |    17   | 0x03 or 0x04 |
| Power Factor 1         |    37   | 0x03 or 0x04 |
| Power Factor 2         |    39   | 0x03 or 0x04 |
| Power Factor 3         |    41   | 0x03 or 0x04 |
| Active energy imported |   801   | 0x03 or 0x04 |


## Software solution descritpion

The example provided by Microsoft in the repo uses a development board with multiple sensors attached, read them and finally send them to the Azure IoT central. With this in mind, the ModBus routines have been setup as another sensor, so the starting application code does not need to be changed too much.

For the ModBus client part, [ESP-IDF FreeModBus](https://github.com/espressif/esp-idf/tree/v4.3/examples/protocols/modbus/tcp/mb_tcp_master) has been integrated into the app, with the following modifications:

* Since the above code was a standalone app, some initialization code must be removed, like WiFi,UART and FileSystem initializations, as the main app already does that.

* Amount of float registers increased, the example provided only 8 in two consecutive banks (0-3 and 158-162). 

* Removal of code related to communication with clients via local domain names (mDNS). 

### How to change parameters:

* To change the IP addresses of the servers, the structure ```slave_ip_address_table``` in [sensor_manager.c](https://gitlab.com/alnunez/fw-ttest-alberto-nunez/-/blob/iot_solution_alberto_nunez/demos/projects/ESPRESSIF/aziotkit/components/azure-iot-kit-sensors/src/sensor_manager.c) must be modified:

```	
	char* slave_ip_address_table[MB_DEVICE_COUNT] = {
	"192.168.20.30",     // Address corresponds to MB_DEVICE_ADDR1 and set to predefined value by user
	"192.168.20.31",
	"192.168.20.32",
	"192.168.20.71",
	NULL                // Marker of end of list
	};
```
Each of these addresses needs to be mapped in the following structure as a means of identifier:

```
// Enumeration of modbus device addresses accessed by master device
// Each address in the table is a index of TCP slave ip address in mb_communication_info_t::tcp_ip_addr table
enum {
    MB_DEVICE_ADDR1 = 1, // Slave address 1
    MB_DEVICE_ADDR2,
    MB_DEVICE_ADDR3,
    MB_DEVICE_ADDR4,
    MB_DEVICE_COUNT
};
```
* To map the ModBus registers to be read, an entry for each register, with a unique name should be placed in the following structure

```
/ Enumeration of all supported CIDs for device (used in parameter definition table)
enum {
    CID_INP_DATA_0_0 = 0,    
    CID_INP_DATA_1_0,
    CID_INP_DATA_2_0,
    CID_INP_DATA_4_0,
    CID_INP_DATA_5_0,
    CID_INP_DATA_6_0,
    CID_INP_DATA_16_0,
    CID_INP_DATA_17_0,
    CID_INP_DATA_18_0,    
    CID_INP_DATA_19_0,   
    CID_INP_DATA_0_1,   
    CID_INP_DATA_1_1,   
    CID_INP_DATA_2_1,  
    CID_INP_DATA_4_1,
    CID_INP_DATA_5_1,
    CID_INP_DATA_6_1, 
    CID_INP_DATA_16_1,
    CID_INP_DATA_17_1,
    CID_INP_DATA_18_1,    
    CID_INP_DATA_19_1,  
    CID_INP_DATA_0_2,   
    CID_INP_DATA_1_2,   
    CID_INP_DATA_2_2,  
    CID_INP_DATA_4_2,
    CID_INP_DATA_5_2,
    CID_INP_DATA_6_2, 
    CID_INP_DATA_16_2,
    CID_INP_DATA_17_2,
    CID_INP_DATA_18_2,    
    CID_INP_DATA_19_2,
    CID_INP_DATA_0_3,   
    CID_INP_DATA_1_3,   
    CID_INP_DATA_2_3,  
    CID_INP_DATA_4_3,
    CID_INP_DATA_5_3,
    CID_INP_DATA_6_3, 
    CID_INP_DATA_16_3,
    CID_INP_DATA_17_3,
    CID_INP_DATA_18_3,    
    CID_INP_DATA_19_3,      
    CID_COUNT
};

```
Finally, you have to create (or modify) a dictionary according to the following structure:

```
/ Example Data (Object) Dictionary for Modbus parameters:
// The CID field in the table must be unique.
// Modbus Slave Addr field defines slave address of the device with correspond parameter.
// Modbus Reg Type - Type of Modbus register area (Holding register, Input Register and such).
// Reg Start field defines the start Modbus register number and Reg Size defines the number of registers for the characteristic accordingly.
// The Instance Offset defines offset in the appropriate parameter structure that will be used as instance to save parameter value.
// Data Type, Data Size specify type of the characteristic and its data size.
// Parameter Options field specifies the options that can be used to process parameter value (limits or masks).
// Access Mode - can be used to implement custom options for processing of characteristic (Read/Write restrictions, factory mode values and etc).
const mb_parameter_descriptor_t device_parameters[] = {
    // { CID, Param Name, Units, Modbus Slave Addr, Modbus Reg Type, Reg Start, Reg Size, Instance Offset, Data Type, Data Size, Parameter Options, Access Mode}
    { CID_INP_DATA_0_0, STR("Voltage_l1_n_0"), STR("Volts"), MB_DEVICE_ADDR1, MB_PARAM_INPUT, 0, 2,
                    INPUT_OFFSET(input_data0), PARAM_TYPE_FLOAT, 4, OPTS( -250, 250, 0 ), PAR_PERMS_READ_WRITE_TRIGGER },
    { CID_INP_DATA_1_0, STR("Voltage_l2_n_0"), STR("Volts"), MB_DEVICE_ADDR1, MB_PARAM_INPUT, 2, 2,
                    INPUT_OFFSET(input_data1), PARAM_TYPE_FLOAT, 4, OPTS( -250, 250, 0 ), PAR_PERMS_READ_WRITE_TRIGGER }
};                    
```
Once set up is complete, the function ``` master_operation_func(float * mb_table, uint32_t mb_size) ``` must be called for ModBus poll. Each of the registers will be written in mb_table in the order specified in the dictionary.

Once the table is filled with the variables from the meters, the data is packed into a json structure to be sent to Azure IoT clod. The tags used for the variables can be modified in [azure_iot_freertos_esp32_sensors_data.c](https://gitlab.com/alnunez/fw-ttest-alberto-nunez/-/blob/iot_solution_alberto_nunez/demos/projects/ESPRESSIF/aziotkit/main/azure_iot_freertos_esp32_sensors_data.c) file. 

```
#define sampleazureiotTELEMETRY_VOLTAGE_L1_N_01    ( "v1_1" )
#define sampleazureiotTELEMETRY_VOLTAGE_L2_N_01    ( "v2_1" )
#define sampleazureiotTELEMETRY_VOLTAGE_L3_N_01    ( "v3_1" )
#define sampleazureiotTELEMETRY_CURRENT_L1_01      ( "i1_1" )
#define sampleazureiotTELEMETRY_CURRENT_L2_01      ( "i2_1" )
#define sampleazureiotTELEMETRY_CURRENT_L3_01      ( "i3_1" )
#define sampleazureiotTELEMETRY_POWERFACTOR_L1_01  ( "p1_1" )
#define sampleazureiotTELEMETRY_POWERFACTOR_L2_01  ( "p2_1" )
#define sampleazureiotTELEMETRY_POWERFACTOR_L3_01  ( "p3_1" )
#define sampleazureiotTELEMETRY_ACCUENERGYIMP_01   ( "aei_1" )
```
If some entry on the telemetry transmitted needs to be modified, deleted, or added, this is the place: ``` uint32_t ulSampleCreateTelemetry( uint8_t * pucTelemetryData,uint32_t ulTelemetryDataLength )```

Each tag ends with a number representing the meter (process) where it came from i.e.:  ```"i1_1"``` means current 1 from meter 1, ```"v3_2"``` means voltage line 3 from meter 2, and so on.

## How to build this project

A cloud app in Azure IoT central must be created, please follow the [Azure IoT kit guide](https://learn.microsoft.com/en-us/azure/iot-develop/quickstart-devkit-espressif-esp32-freertos), but with some "tweaks" as follows:

* The project works with **ESP-IDF v4.3**. So please install this exact version, instead the one stated in the guide. For a complete installation guide, please see the following link: [ESP-IDF v4.3 getting started](https://docs.espressif.com/projects/esp-idf/en/v4.3/esp32/get-started/index.html)

* The project also used a different IoT template, so create a new one importing the [following file](/azure-templates/ESP32_AZURE_ENERGY_METER.json), and associate the ESP-32 device to the new one.

Clone the repo:

	git clone --recursive https://gitlab.com/alnunez/fw-ttest-alberto-nunez

Switch to iot_solution_alberto_nunez branch:

	git checkout iot_solution_alberto_nunez

go to folder demos/projects/ESPRESSIF/aziotkit/

	cd demos/projects/ESPRESSIF/aziotkit/

To configure project (specially to set provisioning keys) type:

	idf.py menuconfig

Input the provisioning keys and WiFi credentials as shown in the [guide](https://learn.microsoft.com/en-us/azure/iot-develop/quickstart-devkit-espressif-esp32-freertos))


To build the project type:

	idf.py build
	
To flash the ESP32 device type:

	idf.py -p PORT flash

Depending on the operating systems, some permissions need to be granted, so the idf tool can access USB ports. Some [linux hints here](https://askubuntu.com/questions/112568/how-do-i-allow-a-non-default-user-to-use-serial-device-ttyusb0).

If everything went well (as i hope!) you are now able to see the data in you Azure IoT central.


"It ain't much, but it's honest work.".

